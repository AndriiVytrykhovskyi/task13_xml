package com.vytrykhovskyi.model;

public class Parameters {
    private String color;
    private int transparency;
    private int facesNumber;

    public Parameters(String color, int transparency, int facesNumber) {
        this.color = color;
        this.transparency = transparency;
        this.facesNumber = facesNumber;
    }

    public Parameters() {

    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getTransparency() {
        return transparency;
    }

    public void setTransparency(int transparency) {
        this.transparency = transparency;
    }

    public int getFacesNumber() {
        return facesNumber;
    }

    public void setFacesNumber(int facesNumber) {
        this.facesNumber = facesNumber;
    }

    @Override
    public String toString() {
        return "Parameters{" +
                "color='" + color + '\'' +
                ", transparency=" + transparency +
                ", facesNumber=" + facesNumber +
                '}';
    }
}
