package com.vytrykhovskyi.parser.stax;

import com.vytrykhovskyi.model.Gems;
import com.vytrykhovskyi.model.Parameters;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class StAXReader {
    public static List<Gems> parseGems(File xml, File xsd) {
        List<Gems> gemsList = new ArrayList<>();
        Gems gems = null;
        Parameters parameters = null;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    switch (name) {
                        case "gems":
                            gems = new Gems();

                            Attribute idAttr = startElement.getAttributeByName(new QName("gemsNumber"));
                            if (idAttr != null) {
                                gems.setGemsNumber(Integer.parseInt(idAttr.getValue()));
                            }
                            break;
                        case "name":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert gems != null;
                            gems.setName(xmlEvent.asCharacters().getData());
                            break;
                        case "origin":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert gems != null;
                            gems.setOrigin(xmlEvent.asCharacters().getData());
                            break;
                        case "preciousness":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert gems != null;
                            gems.setPreciousness(xmlEvent.asCharacters().getData());
                            break;
                        case "value":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert gems != null;
                            gems.setValue(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "parameters":
                            xmlEvent = xmlEventReader.nextEvent();
                            parameters = new Parameters();
                            break;
                        case "color":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert parameters != null;
                            parameters.setColor(xmlEvent.asCharacters().getData());
                            break;
                        case "transparency":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert parameters != null;
                            parameters.setTransparency(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "facesNumber":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert parameters != null;
                            parameters.setFacesNumber(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                    }
                }

                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("gems")) {
                        gemsList.add(gems);
                    }
                }
            }

        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return gemsList;
    }
}
