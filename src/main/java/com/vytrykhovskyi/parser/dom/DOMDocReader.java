package com.vytrykhovskyi.parser.dom;

import com.vytrykhovskyi.model.Gems;
import com.vytrykhovskyi.model.Parameters;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.util.ArrayList;
import java.util.List;

public class DOMDocReader {
    public List<Gems> readDoc(Document doc) {
        doc.getDocumentElement().normalize();
        List<Gems> gemses = new ArrayList<>();

        NodeList nodeList = doc.getElementsByTagName("gems");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Gems gems = new Gems();
            Parameters parameters;

            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;

                gems.setGemsNumber(Integer.parseInt(element.getAttribute("gemsNumber")));
                gems.setName(element.getElementsByTagName("name").item(0).getTextContent());
                gems.setOrigin(element.getElementsByTagName("origin").item(0).getTextContent());
                gems.setPreciousness(element.getElementsByTagName("preciousness").item(0).getTextContent());
                gems.setValue(Integer.parseInt(element.getElementsByTagName("value").item(0).getTextContent()));

                parameters = getParameters(element.getElementsByTagName("parameters"));

                gems.setParameters(parameters);
                gemses.add(gems);
            }
        }
        return gemses;
    }

    private Parameters getParameters(NodeList nodes) {
        Parameters parameters = new Parameters();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            parameters.setColor(element.getElementsByTagName("color").item(0).getTextContent());
            parameters.setFacesNumber(Integer.parseInt(element.getElementsByTagName("facesNumber").item(0).getTextContent()));
            parameters.setTransparency(Integer.parseInt(element.getElementsByTagName("transparency").item(0).getTextContent()));
        }
        return parameters;
    }
}
