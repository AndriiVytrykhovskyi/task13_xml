package com.vytrykhovskyi.parser.sax;

import com.vytrykhovskyi.model.Gems;
import com.vytrykhovskyi.model.Parameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SAXHandler extends DefaultHandler {
    private List<Gems> gemsList = new ArrayList<>();
    private Gems gems = null;

    private Parameters parameters = null;

    private boolean bName = false;
    private boolean bOrigin = false;
    private boolean bPrec = false;
    private boolean bValue = false;

    private boolean bPar = false;
    private boolean bColor = false;
    private boolean bTrans = false;
    private boolean bFacesN = false;


    public List<Gems> getGemsList() {
        return this.gemsList;
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("gems")) {
            String gemsN = attributes.getValue("gemsNumber");
            gems = new Gems();
            gems.setGemsNumber(Integer.parseInt(gemsN));
        } else if (qName.equalsIgnoreCase("name")) {
            bName = true;
        } else if (qName.equalsIgnoreCase("origin")) {
            bOrigin = true;
        } else if (qName.equalsIgnoreCase("preciousness")) {
            bPrec = true;
        } else if (qName.equalsIgnoreCase("value")) {
            bValue = true;
        } else if (qName.equalsIgnoreCase("parameters")) {
            bPar = true;
        } else if (qName.equalsIgnoreCase("color")) {
            bColor = true;
        } else if (qName.equalsIgnoreCase("transparency")) {
            bTrans = true;
        } else if (qName.equalsIgnoreCase("facesNumber")) {
            bFacesN = true;
        }

    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("gems")) {
            gemsList.add(gems);
        }
    }

    public void characters(char ch[], int start, int length) throws SAXException {
        if (bName) {
            gems.setName(new String(ch, start, length));
            bName = false;
        } else if (bOrigin) {
            gems.setOrigin(new String(ch, start, length));
            bOrigin = false;
        } else if (bPrec) {
            gems.setPreciousness(new String(ch, start, length));
            bPrec = false;
        } else if (bValue) {
            int v = Integer.parseInt(new String(ch, start, length));
            gems.setValue(v);
            bValue = false;
        } else if (bPar) {
            parameters = new Parameters();
            bPar = false;
        } else if (bColor) {
            String alc = new String(ch, start, length);
            parameters.setColor(alc);
            bColor = false;
        } else if (bTrans) {
            int trans = Integer.parseInt(new String(ch, start, length));
            parameters.setTransparency(trans);
            bTrans = false;
        } else if (bFacesN) {
            int filt = Integer.parseInt(new String(ch, start, length));
            parameters.setFacesNumber(filt);
            bFacesN = false;
        }
    }
}

