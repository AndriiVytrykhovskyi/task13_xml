package com.vytrykhovskyi.parser;

import com.vytrykhovskyi.comparator.GemsComparator;
import com.vytrykhovskyi.model.Gems;
import com.vytrykhovskyi.parser.dom.DOMParserUser;
import com.vytrykhovskyi.filechecker.Checker;
import com.vytrykhovskyi.parser.sax.SAXParserUser;
import com.vytrykhovskyi.parser.stax.StAXReader;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.List;

public class App {
    public static void main(String[] args) {
        File xml = new File("src\\main\\resources\\xml\\gemsXML.xml");
        File xsd = new File("src\\main\\resources\\xml\\gemsXSD.xsd");

        if (checkIfXML(xml) && checkIfXSD(xsd)) {
//            SAX
            printList(SAXParserUser.parseGems(xml, xsd), "SAX");

//            StAX
            printList(StAXReader.parseGems(xml, xsd), "StAX");

//            DOM
            printList(DOMParserUser.getGemsList(xml, xsd), "DOM");
        }
        convertIntoHTML();
    }

    private static boolean checkIfXSD(File xsd) {
        return Checker.isXSD(xsd);
    }

    private static boolean checkIfXML(File xml) {
        return Checker.isXML(xml);
    }

    private static void printList(List<Gems> gems, String parserName) {
        Collections.sort(gems, new GemsComparator());
        System.out.println("\n" + parserName);
        for (Gems g : gems) {
            System.out.println(g);
        }
    }

    private static void convertIntoHTML() {
        try {
            Source xslDoc = new StreamSource("src\\main\\resources\\xml\\gemsXSLT.xsl");
            Source xmlDoc = new StreamSource("src\\main\\resources\\xml\\gemsXML.xml");
            OutputStream htmlFile = new FileOutputStream("GemsInfo.html");

            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transform = tFactory.newTransformer(xslDoc);
            transform.transform(xmlDoc, new StreamResult(htmlFile));

            System.out.println("\nOpen GemsInfo.html");
        } catch (FileNotFoundException | TransformerFactoryConfigurationError | TransformerException e) {
            e.printStackTrace();
        }
    }
}

