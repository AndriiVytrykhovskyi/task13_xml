<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <html>
            <head>
                <style type="text/css">
                    table.tfmt {
                    border: 1px ;
                    }

                    td.colfmt {
                    border: 1px ;
                    background-color: yellow;
                    color: red;
                    text-align:right;
                    }

                    th {
                    background-color: #2E9AFE;
                    color: white;
                    }
                </style>
            </head>
            <body>
                <h1>Gems Info</h1>
                <table class="tfmt">
                    <tr>
                        <th style="width:250px">name</th>
                        <th style="width:250px">origin</th>
                        <th style="width:250px">preciousness</th>
                        <th style="width:250px">color</th>
                        <th style="width:250px">transparency</th>
                        <th style="width:250px">faces</th>
                        <th style="width:250px">value</th>
                    </tr>

                    <xsl:for-each select="gemses/gems">
                        <xsl:sort select="value" data-type="number"/>
                        <tr>
                            <td class="colfmt">
                                <xsl:value-of select="name"/>
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="origin"/>
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="preciousness"/>
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="parameters/color"/>
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="parameters/transparency"/>
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="parameters/facesNumber"/>
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="value"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>